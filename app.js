const express = require('express');
const bodyParser = require('body-parser');
const Ledger = require('./ledger'); 
const http_port = 3000;

//Start first ledger off on a random port
const port = 18070 + Math.floor(Math.random() * 30);
console.log('starting ledger on ', port)
let ledger1 = new Ledger(port);

ledger1.init();

let ChainHTTP = function (){
	const app = new express();

	app.use(bodyParser.json());

  //Spin up a new ledger
	app.get('/addLedger/:port', (req, res)=>{
    let newNode = new Ledger(req.params.port);
    newNode.init();
		ledger1.addPeer('localhost', req.params.port)
    newNode.addPeer('localhost', port);

		res.send();
	})

  //Create a new block
  //Simulating a ticket purchase online
  app.post('/tickets/new', (req, res) => {
    const user = req.body.userId;
    const name = req.body.name;

    let newBlock = ledger1.createBlock({user, name});
    console.log("Block created: ", newBlock);
    res.send();
  });

  app.get('/chain/view', (req, res) => {
    res.json(ledger1.getStats()); 
  });

	app.listen(http_port, () => {
		console.log(`http server up.. ${http_port}`);
	})
}

let httpserver = new ChainHTTP();
