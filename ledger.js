const Blockchain = require('./blockchain');
const WebSocket = require('ws');

const Ledger = function(port){
  let sockets = [];
  let server;
  //Internal variable
  let _port = port;
  //For logging purposes
  let _name = `NODE-${_port}`;
  let chain = new Blockchain();

  //Constants for our message handling DSL
  const REQUEST_CHAIN = "REQUEST_CHAIN";
  const REQUEST_BLOCK = "REQUEST_BLOCK";
  const BLOCK = "BLOCK";
	const CHAIN = "CHAIN";

  function init(){
    chain.init();

    server = new WebSocket.Server({ port: _port });

    server.on('connection', (connection) => {
      logger('connection in');
      initConnection(connection);
    });		
  }

  //Custom logging function to allow us to see which ledger node
  //is doing which action
  function logger(data) {
    console.log(_name, data); 
  }

  //This is our custom DSL for handling messages in and out of the ledger
  const messageHandler = (connection) =>{
    connection.on('message', (data) => {
      const payload = JSON.parse(data);
      switch(payload.cmd){
        case REQUEST_CHAIN:
          connection.send(JSON.stringify({ cmd: CHAIN, data: chain.getChain()}))    
        break;                  
        case REQUEST_BLOCK:
          requestLatestBlock(connection);
        break;      
        case BLOCK:
          processedRecievedBlock(payload.data);
        break;  
        case CHAIN:
          processedRecievedChain(payload.data);
        break;  

        default:  
          logger('Unknown message ');
      }
    });
  }


    const processedRecievedChain = (blocks) => {
      //Sanity check
      let newChain = blocks.sort((block1, block2) => (block1.index - block2.index))

      if(newChain.length > chain.getTotalBlocks() && chain.checkNewChainIsValid(newChain)){
        chain.replaceChain(newChain);
        logger('chain replaced');
      }
    }

    const processedRecievedBlock = (block) => {

      let currentTopBlock = chain.getLatestBlock();

      // Is the same or older?
      if(block.index <= currentTopBlock.index){
        logger('No update needed');
        return;
      }

      //Is claiming to be the next in the chain
      if(block.previousHash == currentTopBlock.hash){
        //Attempt the top block to our chain
        chain.addToChain(block);

        logger('New block added');
        logger(chain.getLatestBlock());
      } else {
        // It is ahead.. we are therefore a few behind, request the whole chain
        logger('requesting chain');
        broadcast(REQUEST_CHAIN,"");
      }
    }

    //Standalone method to do this, as we have to do this
    //action multiple times
    const requestLatestBlock = (connection) => {
      connection.send(JSON.stringify({ event: BLOCK, message: chain.getLatestBlock()}))   
    }

    //Function for sending messages to all connected peer ledgers
    const broadcast = (cmd, data) => {
      sockets.forEach(node => {
        if (node.readyState === WebSocket.OPEN) { 
          node.send(JSON.stringify({ cmd, data}))
        }
      });
    }

    //Connection handling
    const closeConnection = (connection) => {
      logger('closing connection');
      sockets.splice(sockets.indexOf(connection),1);
    }

    //Setup/initialization function for a new connection
    const initConnection = (connection) => {
      logger('init connection');

      messageHandler(connection);
      
      requestLatestBlock(connection);

      sockets.push(connection);

      connection.on('error', () => closeConnection(connection));
      connection.on('close', () => closeConnection(connection));
    }

    //Interface method for creating a block through the ledger
    const createBlock = (data) => {
      let newBlock = chain.createBlock(data)
      chain.addToChain(newBlock);

      broadcast(BLOCK, newBlock);
    }

    //Just an info function that reports on the
    //state of the data stored in the blockchain
    const getStats = () => {
      return {
        blocks: chain.getTotalBlocks(),
        chain: chain.getChain()
      }
    }

    //Adding a peer to the distributed ledger system
    const addPeer = (host, port) => {
      let connection = new WebSocket(`ws://${host}:${port}`);

      connection.on('error', (error) =>{
          logger(error);
      });

      connection.on('open', (msg) =>{
        initConnection(connection);
      });
    }

    return {
      init,
      broadcast,
      addPeer,
      createBlock,
      getStats
    };
}

module.exports = Ledger;
